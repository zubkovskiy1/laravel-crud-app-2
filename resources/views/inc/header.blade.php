<!doctype html>
<html lang="en">
<head>
    <title>laravel CRUD</title>
    <link rel="stylesheet" href="{{ url('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand" href="{{ url('/') }}">Laravel CRUD</a>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ (\Request::is('dashboard') ? 'active' : '') }}">
                <a class="nav-link" href="{{ url('/dashboard') }}" >Dashboard</a>
            </li>
            @auth
            <li class="nav-item {{ (\Request::is('create') ? 'active' : '') }}">
                <a class="nav-link" href="{{ url('/create') }}" >Create </a>
            </li>
            @endauth
        </ul>
    </div>

    <form class="navbar-form" role="search" method="get" action="{{url("/search")}}">
        <div class="input-grup search">
            <input type="text" class="form-control mr-sm-2 " placeholder="search" name="title">
            <div class="input-group-btn">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">search</button>
            </div>
        </div>
    </form>

</nav>