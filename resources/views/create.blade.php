@extends('layouts.index')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <form method="POST" action="{{ url('/insert') }}">
                    {{csrf_field()}}
                    <fieldset>
                        <legend>Laravel</legend>

                        @if(count($errors) > 0 )
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger">
                                    {{ $error }}
                                </div>
                            @endforeach
                        @endif

                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="title">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Description</label>
                            <textarea name="description" class="form-control" placeholder="description" name="" id="" cols="30" rows="10"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="{{ url('/dashboard') }}" class="btn btn-primary">Back</a>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection









