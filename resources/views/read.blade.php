@extends('layouts.index')
@section('content')
        <div class="container">
                <legend >Read Article</legend>
                <div class="line"></div>
                <p class="display-4">{{ $articles->title }}</p>
                <p class="lead">{{ $articles->description }}</p>
                <div class="line"></div>
                <a href="{{ url('/dashboard') }}" class="btn btn-primary">Back</a>
        </div>
@endsection