@extends('layouts.index')
@section('content')
    <div class="container">
        <div class="row">
            @if(session('info'))
                <div class="alert alert-success no-found">
                    {{ session('info') }}
                </div>
            @endif

        </div>
    </div>
@endsection
