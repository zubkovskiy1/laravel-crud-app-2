<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class CreatesController extends Controller
{

    public function welcome()
    {
        return view('welcome');

    }

    public function index()
    {
        return view('create');

    }


    public function dashboard()
    {
        $articles = Article::latest()->paginate(3);
        return view('dashboard',['articles' => $articles]);
    }

    public function search()
    {
        $searchkey = \Request::get('title');
        if($searchkey != ""){
            $articles = Article::where('title', 'like', '%' .$searchkey. '%')->orderBy('id')->paginate(5);
            if(count($articles) > 0 ){
                return view('search',['articles' => $articles]);
            }
        }
        return redirect('404')->with('info','No data found!');
    }


    public function add(Request $request)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'description' => 'required'
            ]);
        $articles = new Article;
        $articles -> title = $request->input('title');
        $articles -> description = $request->input('description');
        $articles->save();
        return redirect('/dashboard')->with('info','Article saved successfully');
    }


    public function update($id)
    {
        $articles = Article::find($id);
        return view('/update',['articles' => $articles]);
    }


    public function edit(Request $request,$id)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'description' => 'required'
            ]);
        $data = array(
                'title' => $request->input('title'),
                'description' => $request->input('description')
        );
        Article::where('id',$id)->update($data);
        $articles = new Article;
        return redirect('/dashboard')->with('info','Article update successfully');
    }


    public function read($id)
    {
        $articles = Article::find($id);
        return view('/read',['articles' => $articles]);
    }


    public function delete($id)
    {
        Article::where('id', $id)->delete();
        return redirect('/dashboard')->with('info','Article delete successfully');
    }

    public function no_found()
    {
        return view('404');
    }
}



