<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','CreatesController@welcome');

Route::get('/dashboard','CreatesController@dashboard');

Route::get('/search','CreatesController@search');

Route::get('/create','CreatesController@index');

Route::post('/insert','CreatesController@add');

Route::get('/update/{id}','CreatesController@update');

Route::post('/edit/{id}','CreatesController@edit');

Route::get('/read/{id}','CreatesController@read');

Route::get('/delete/{id}','CreatesController@delete');

Route::get('/404','CreatesController@no_found');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
