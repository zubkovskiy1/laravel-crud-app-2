$(document).ready( function(){
    $(".pagination ").addClass(function() {
        return "pagination-lg";
    });

    $(".pagination li").addClass(function() {
        return "page-item";
    });
    $(".pagination li a").addClass(function() {
        return "page-item page-link";
    });
    $(".pagination li span").addClass(function() {
        return "page-item page-link";
    });
});